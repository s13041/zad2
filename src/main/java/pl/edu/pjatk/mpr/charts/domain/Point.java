package main.java.pl.edu.pjatk.mpr.charts.domain;

/**
 * Created by Krzysztof on 09/01/16.
 */
public class Point {
    private final double x;
    private final double y;

    public Point(double x, double y) {
        this.x = x;
        this.y = y;
    }

    public double getX() {
        return x;
    }

    public double getY() {
        return y;
    }

	public void setX(double x) {
		// TODO Auto-generated method stub
		this.x=x;
	}

	public void setY(double y) {
		// TODO Auto-generated method stub
		this.y=y;
		
	}
}
