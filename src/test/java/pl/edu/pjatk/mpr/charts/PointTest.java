package test.java.pl.edu.pjatk.mpr.charts;

import main.java.pl.edu.pjatk.mpr.charts.domain.Point;

import org.assertj.core.api.Assertions;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import static org.assertj.core.api.Java6Assertions.tuple;
import static org.junit.Assert.*;

public class PointTest {

	private Point testPoint;

	@Before
	public void setPoint() throws Exception {

		testPoint = new Point(2, 3);

	}

	@After
	public void setNull() throws Exception {

		testPoint = null;

	}

	@Test
	public void testPoint_setingAndGettingX() {

		double pointx = 7.0;

		this.testPoint.setX(pointx);

		Assertions.assertThat(testPoint).extracting("x")

		.containsExactly(pointx);

	}

	@Test
	public void testPointSettingInititalValuesAndChangeIt() throws Exception {

		double initX = 5.0;

		double initY = 7.9;

		Point testPoint = new Point(initX, initY);

		Assertions.assertThat(testPoint)

		.extracting("x", "y")

		.containsExactly(initX, initY);

		double newX = 12;

		double newY = 14;

		testPoint.setX(newX);

		testPoint.setY(newY);

		Assertions.assertThat(testPoint)

		.extracting("x", "y")

		.contains(newX, newY)

		.doesNotContain(initX, initY);

	}

	@Test
	public void testPoint_setYValue() {

		Assertions.assertThat(testPoint);

		double y = 89.123;

		this.testPoint.setY(y);

		Assertions.assertThat(testPoint)

		.extracting("y")

		.containsExactly(y);

	}

	@Test
	public void testPoint_readValuesFromDefault() throws Exception {

		Assertions.assertThat(this.testPoint)

		.extracting("x", "y")

		.containsExactly(0.0, 0.0);

	}

}
