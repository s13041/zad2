package test.java.pl.edu.pjatk.mpr.charts;



import main.java.pl.edu.pjatk.mpr.charts.domain.SerieType;

import org.assertj.core.api.Assertions;
import org.junit.Test;




import static org.junit.Assert.*;

public class SerieTypeTest {

    @Test
    public void testSerieTypeLineValue() throws Exception {

        SerieType serieType = SerieType.LINE;



        Assertions.assertThat(serieType)

                .isEqualTo(SerieType.LINE)

                .isNotEqualTo(SerieType.POINT)

                .isNotNull();



    }



    @Test
    public void testSerieTypePointValue() throws Exception {

        SerieType serieType = SerieType.POINT;



        Assertions.assertThat(serieType)

                .isEqualTo(SerieType.POINT)

                .isNotNull()

                .isNotEqualTo(SerieType.BAR);

    }

}