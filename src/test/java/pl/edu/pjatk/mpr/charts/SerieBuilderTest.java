package test.java.pl.edu.pjatk.mpr.charts;

import main.java.pl.edu.pjatk.mpr.charts.SerieBuilder;
import main.java.pl.edu.pjatk.mpr.charts.domain.ChartSerie;
import main.java.pl.edu.pjatk.mpr.charts.domain.Point;

import org.junit.Before;
import org.junit.Test;

import static org.assertj.core.api.Assertions.assertThat;

/**
 * Created by Krzysztof on 09/01/16.
 */
public class SerieBuilderTest {

	private SerieBuilder serieBuilder;

	@Before
	public void before() {
		serieBuilder = new SerieBuilder();
	}

	@Test
	public void testIfPointIsAdded() {
		// GIVEN
		Point point = new Point(1, 2);

		// WHEN
		ChartSerie serie = serieBuilder.addPoint(point).build();

		// THEN
		assertThat(serie.getPoints()).isNotNull().containsOnly(point);
	}

	@Test
	public void testIfLabelAdded() {
		// GIVEN
		String label = "l";

		// WHEN
		ChartSerie serie = serieBuilder.addLabel(label).build();

		// THEN
		assertThat(serie.getLabel()).isEqualTo(label);
	}

	@Test
	public void testSerieBuilderAddPoint() throws Exception {

		Point point = new Point(0, 1);

		this.serieBuilder.addPoint(point);

		Assertions.assertThat(this.serieBuilder.build().getPoints())

		.hasSize(1)

		.isNotEmpty()

		.isNotNull()

		.containsExactly(point)

		.doesNotContain(new Point(0, 1));

	}

	@Test
	public void testSerieBuilderSetLabel() throws Exception {

		String testLabel = "label";

		this.serieBuilder.addLabel(testLabel);

		Assertions.assertThat(this.serieBuilder.build())

		.extracting("label")

		.containsOnly(testLabel)

		.isNotEmpty()

		.isNotNull();

	}
}
